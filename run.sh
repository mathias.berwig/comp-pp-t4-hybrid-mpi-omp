echo +-------------------------------------+
echo +------- Iniciando mágica ------------+
echo +-------------------------------------+
echo +  para executar digite: bash run.sh  +
echo +-------------------------------------+

printf 'MPI + OpenMP\n\n' >> run.txt
for run in 1 2 3 4 5 6 7 8 9 10
do
    for np in 2 3 4
    do
        printf '\t' >> run.txt
        printf $(mpirun -np $np 1 2) >> run.txt
        echo -en "\n\e[1A\rRun: $run/10 NP: $np NT: 2 " 

        printf '\t' >> run.txt
        printf $(mpirun -np $np 1 3) >> run.txt
        echo -en "\n\e[1A\rRun: $run/10 NP: $np NT: 3 " 

        printf '\t' >> run.txt
        printf $(mpirun -np $np 1 4) >> run.txt
        echo -en "\n\e[1A\rRun: $run/10 NP: $np NT: 4 "
    done
	printf '\n' >> run.txt
done
printf '\n' >> run.txt