#include "mpi.h"
#include <omp.h>
#include "tempo.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#define INPUT_FILE "texto.txt"
#define MASTER 0

#define QUERY "DHGK"

int master(int numProc);
void slave();
int checkTextForPatternAtPosition(int textIndex, int textLength, int lengthToCheck, char query[], int queryLength);
static unsigned get_file_size (const char * file_name);
static unsigned char * read_whole_file (const char * file_name);
char *string_reverse(char *dst, const char *src);

int main(int argc,char *argv[])
{
    int procId, numProc, numThr;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProc);
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);

    numThr = atoi(argv[1]);
    omp_set_num_threads(numThr);

    if (numProc < 2)
    {
        printf("\nSet at least 2 processes. Master doesn't do hard work!\n");
    }
    else if (procId == MASTER)
    {
        tempo1();
        int totalFound = master(numProc);
        tempo2();
        printf("%d|", totalFound);
        tempoFinal("mili segundos", argv[0], MSGLOG);
    }
    else
    {
        slave();
    }
    
    MPI_Finalize();
}

int master(int numProc)
{
    // Move file pointer to end of stream and get text length
    FILE *fp;
    fp = fopen (INPUT_FILE,"r");
    fseek(fp, 0L, SEEK_END);
    const int textLength = ftell(fp);
    fclose(fp);

    // Master won't do hard work
    const int slaves = numProc - 1;

    // Work division
    int count = textLength / slaves;
    int remainder = textLength % slaves;
    int start, stop;
    for (int slaveId = 0; slaveId < slaves; slaveId++)
    {
        if (slaveId < remainder) {
            // The first 'remainder' slave get 'count + 1' tasks each
            start = slaveId * (count + 1);
            stop = start + count;
        } else {
            // The remaining 'size - remainder' slaves get 'count' task each
            start = slaveId * count + remainder;
            stop = start + (count - 1);
        }
        
        // Prepare data to send through MPI
        int control[] = {start, textLength, stop - start};

        MPI_Send(control, 3, MPI_INT, slaveId + 1, 4, MPI_COMM_WORLD);
    }

    MPI_Status status;
    int totalFound = 0, found;
    for (int procId = 1; procId < numProc; procId++)
    {
        MPI_Recv(&found, 1, MPI_INT, procId, 4, MPI_COMM_WORLD, &status);
        totalFound += found;
    }

    return totalFound;
}

void slave()
{
    int control[3];
    MPI_Status status;
    MPI_Recv(control, 3, MPI_INT, 0, 4, MPI_COMM_WORLD, &status);

    const int start = control[0];
    const int textLength = control[1];
    const int lengthToCheck = control[2];
    const int queryLength = strlen(QUERY);
    char reversedQuery[queryLength];
    string_reverse(reversedQuery, QUERY);

    // search the query and its reversed version
    int found = checkTextForPatternAtPosition(start, textLength, lengthToCheck, QUERY, queryLength) + 
                checkTextForPatternAtPosition(start, textLength, lengthToCheck, reversedQuery, queryLength);
    
    MPI_Send(&found, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);
}

// Find pattern at given text position and the positions following for lengthToCheck
int checkTextForPatternAtPosition(int textIndex, int textLength, int lengthToCheck, char query[], int queryLength)
{
    unsigned char * text;
    text = read_whole_file (INPUT_FILE);

	int i;
	int found = 0;
    
    #pragma omp paralell(i) shared(found)
	for (i = 0; i < lengthToCheck; i++)
	if (textLength - (textIndex + i) >= queryLength)
    {
        int k = textIndex + i;
        int j = 0;
            
        while (text[k] == query[j] && j < queryLength)
        {
            k++;
            j++;
        }
        
        if (j == queryLength) found++;
    }

    // Release memory
    free(text);
		
	return found;
}

/* This routine returns the size of the file it is called with. */
static unsigned
get_file_size (const char * file_name)
{
    struct stat sb;
    if (stat (file_name, & sb) != 0) {
        fprintf (stderr, "'stat' failed for '%s': %s.\n",
                 file_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    return sb.st_size;
}

/* This routine reads the entire file into memory. */
static unsigned char *
read_whole_file (const char * file_name)
{
    unsigned s;
    unsigned char * contents;
    FILE * f;
    size_t bytes_read;
    int status;

    s = get_file_size (file_name);
    contents = malloc (s + 1);
    if (! contents) {
        fprintf (stderr, "Not enough memory.\n");
        exit (EXIT_FAILURE);
    }

    f = fopen (file_name, "r");
    if (! f) {
        fprintf (stderr, "Could not open '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    bytes_read = fread (contents, sizeof (unsigned char), s, f);
    if (bytes_read != s) {
        fprintf (stderr, "Short read of '%s': expected %d bytes "
                 "but got %d: %s.\n", file_name, s, bytes_read,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    status = fclose (f);
    if (status != 0) {
        fprintf (stderr, "Error closing '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    return contents;
}

char *string_reverse(char *dst, const char *src)
{
    if (src == NULL) return NULL;

    const char *src_start = src;
    char *dst_end = dst + strlen(src);
    *dst_end = '\0';

    while ((*--dst_end = *src_start++)) { ; }

    return dst;
}