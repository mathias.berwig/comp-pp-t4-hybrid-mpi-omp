# TITULO

Mathias Henrique Nast Berwig

## INTRODUÇÃO

Este trabalho é produto da avaliação final da disciplina de Processamento Paralelo, ministrada pelo professor Edson Luiz Padoin no Curso de Ciência da Computação na Universidade Regional do Noroeste do Estado do Rio Grande do Sul. O mesmo tem o objetivo de demonstrar os resultados atingidos na resolução de um problema de otimização de busca textual.

Um algoritmo de busca textual consiste, basicamente, na verificação de toda a extensão de dados de uma fonte com o objetivo de encontrar uma cadeia de caracteres. A implementação apresentada neste artigo aborda este problema utilizando as tecnologias de processamento paralelo MPI e OpenMP, cuja definição e introdução foge do escopo definido.

## METODOLOGIA

A metodologia será detalhada explicando aspectos do algoritmo desenvolvido, dividindo as tarefas na divisão de trabalho, aspectos do MPI e OMP.

### Divisão de Trabalho

A divisão de trabalho é feita após obter a quantidade total de caracteres no arquivo por meio do método `fseek`:

```
// Move file pointer to end of stream and get text length
FILE *fp;
fp = fopen (INPUT_FILE,"r");
fseek(fp, 0L, SEEK_END);
const int textLength = ftell(fp);
fclose(fp);
```

Então o total de caracteres é dividido pela quantidade de processos escravos (total de processos - 1). Cada processo recebe uma carga de acordo com o resto da divisão anterior:

```
// Work division
int count = textLength / slaves;
int remainder = textLength % slaves;
int start, stop;
for (int slaveId = 0; slaveId < slaves; slaveId++)
{
    if (slaveId < remainder) {
        // The first 'remainder' slave get 'count + 1' tasks each
        start = slaveId * (count + 1);
        stop = start + count;
    } else {
        // The remaining 'size - remainder' slaves get 'count' task each
        start = slaveId * count + remainder;
        stop = start + (count - 1);
    }
    
    ...
}
```

### MPI

O MPI é inicializado a partir do trecho que obtém o número total de processos e o identificador do processo atual:

```
int procId, numProc, numThr;

MPI_Init(&argc, &argv);
MPI_Comm_size(MPI_COMM_WORLD, &numProc);
MPI_Comm_rank(MPI_COMM_WORLD, &procId);
```

A partir disso, sabe-se se o processo é o mestre (`procId == 0`), então é feita a divisão de trabalho, em que, após determinar as posições `start` e `end` (início e fim do trabalho, respectivamente) são transmitidas aos processos escravos:

```
// Prepare data to send through MPI
int control[] = {start, textLength, stop - start};

MPI_Send(control, 3, MPI_INT, slaveId + 1, 4, MPI_COMM_WORLD);
```

Após a notificação de todos os escravos, o mestre aguarda pelo resultado do trabalho, onde soma o número de itens encontrados: 

```
MPI_Status status;
int totalFound = 0, found;
for (int procId = 1; procId < numProc; procId++)
{
    MPI_Recv(&found, 1, MPI_INT, procId, 4, MPI_COMM_WORLD, &status);
    totalFound += found;
}
```

O escravo recebe o trabalho e executa a busca textual, retornando o resultado ao mestre:

```
void slave()
{
    int control[3];
    MPI_Status status;
    MPI_Recv(control, 3, MPI_INT, 0, 4, MPI_COMM_WORLD, &status);

    const int start = control[0];
    const int textLength = control[1];
    const int lengthToCheck = control[2];
    const int queryLength = strlen(QUERY);
    char reversedQuery[queryLength];
    string_reverse(reversedQuery, QUERY);

    // do the search
    int found;
    
    MPI_Send(&found, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);
}
```

### OMP 

A paralelização via OpenMP é executada diretamente na busca, após receber os parâmetros de divisão de trabalho via MPI. A definição do número de threads do OpenMP é feita via argumento de inicialização da aplicação:

```
numThr = atoi(argv[1]);
omp_set_num_threads(numThr);
```

Com o objetivo de otimizar ao máximo a consulta, foi implementado o carregamento do texto em tamanho integral para a memória RAM, o que é feito pelo método `read_whole_file`. A divisão de trabalho via OpenMP é feita de modo que cada thread receba uma fatia de trabalho a partir dos parâmetros informados via MPI - na prática, o arquivo é divido duas vezes, uma para criar processos e outra para criar as threads. 

```
// Find pattern at given text position and the positions following for lengthToCheck
int checkTextForPatternAtPosition(int textIndex, int textLength, int lengthToCheck, char query[], int queryLength)
{
    unsigned char * text;
    text = read_whole_file (INPUT_FILE);

	int i;
	int found = 0;
    
    #pragma omp paralell(i) shared(found)
	for (i = 0; i < lengthToCheck; i++)
	if (textLength - (textIndex + i) >= queryLength)
    {
        int k = textIndex + i;
        int j = 0;
            
        while (text[k] == query[j] && j < queryLength)
        {
            k++;
            j++;
        }
        
        if (j == queryLength) found++;
    }

    // Release memory
    free(text);
		
	return found;
}
```

## RESULTADOS E DISCUSSÃO

Para validar a otimização do processo, foram mensurados os tempos de execução do algoritmo em suas versões sequencial (sem qualquer otimização paralela); com MPI (leitura dos caracteres diretamente no disco); MPI e OMP (com leitura dos caracteres na memória RAM).

O speed-up de uma execução sequencial com leituras diretas no disco rígido para o carregamento do programa na memória e leituras paralelas chega ao patamar de 45 vezes. Isso faz com que o programa que antes durava cerca de dois minutos e meio para ser executado seja concluído em menos de 5 segundos. É importante notar que a maior parte do ganho dessa abordagem se dá pelo uso da leitura em memória, e não somente pelo uso de paralelismo. 

O cabeçalho da tabela mostra o número de processos e threads (2P 2T significam 2 processos e 2 threads), e o número logo abaixo o total de threads efetivamente calculando (`(número de processos - 1) * número de threads por processo`), visto que o mestre apenas realiza a divisão de trabalho. As execuções ótimas para a configuração em que os testes foram realizados (laptop Dell XPS 13 9360 com processador Core i5 7200U @2.5Ghz x 4, 8GB de RAM e SSD NVMe 256GB) foram com 4P 2T e 4P 4T, sendo este último o mais rápido.

### Tempos de Execução para TXT de 250MB com MPI e OpenMP

![Tabela de Tempos de Execução para TXT de 250MB com MPI e OpenMP](images/table-mpi-omp-250mb.png)  

![Gráfico de Tempos de Execução, Speed-up e Eficiência para TXT de 250MB com MPI e OpenMP](images/chart-mpi-omp-250mb.png)

### Tabela de Tempos de Execução para TXT de 1000MB com MPI e OpenMP

![Tabela de Tempos de Execução para TXT de 1000MB com MPI e OpenMP](images/table-mpi-omp-1000mb.png)

![Gráfico de Tempos de Execução, Speed-up e Eficiência para TXT de 1000MB com MPI e OpenMP](images/chart-mpi-omp-1000mb.png)

## CONSIDERAÇÕES FINAIS

É possível alcançar um alto grau de otimização por meio da utilização do paralelismo, tornando a resolução de um problema muito mais rápida. Para otimizar o processo de solução do problema é importante analisar as possibilidades de divisão do problema em fatias que possam ser destinadas aos processos e threads encarregados de tal trabalho.  

O ganho de desempenho mais notável atingido por este algoritmo se deu em razão do carregamento do texto na memória - assume-se que o computador possui memória suficiente para alocar o documento inteiro. As implementações anteriores que utilizam leituras no disco rígido alcançaram um speed-up de cerca de 3,4 vezes, quando comparados com a sua versão sequencial. Os resultados apresentados estariam no mesmo patamar caso não houvesse uso da memória RAM para alocação integral do texto.