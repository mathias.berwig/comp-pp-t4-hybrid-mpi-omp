all: compile
	
compile:
	mpicc -o 1 hybrid.c -fopenmp

clean:
	rm -rf ?
	rm -rf *.txt
	rm -rf *.out