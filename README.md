## Resumo

Trabalho IV - Programação Hibrida  
Data de entrega: 27/06/18  
Local de entrega: somente no portal  
Peso 50 pontos  
Individual

## Requisitos

Escolher um dos problemas dos trabalhos 1, 2 ou 3 e implementar uma versão
paralela híbrida (memória distribuída + memória compartilhada)

## Entrega do trabalho

- arquivo zipado no portal contendo:
- arquivo (MPI.c) - implementação paralela com MPI
- arquivo (Makefile) - make
- arquivo tabela (spped-up_e_eficiencia.ods) - calculos de speed-up e eficiência com a média de 10 execuções, descrição do computador e comparativo com o trabalho anterior

## Avaliação do trabalho

- 15 pontos - funcionalidade, lógica de paralelização / divisão do trabalho

- 15 pontos - Ganhos de tempo(speed-up) e eficiência obtida com a paralelização utilizando arquivo de 1 GB.

- 20 pontos - artigo no formato do salão do conhecimento - https://www.unijui.edu.br/eventos/salo-do-conhecimento-2018-932